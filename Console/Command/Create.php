<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Syte\ProductFeed\Model\Helper;
use Syte\Core\Model\Constants;

class Create extends Command
{
    /**
     * @var Helper
     */
    private $helper;

    protected function configure()
    {
        $this->setName(Constants::SYTE_PRODUCT_FEED_BASE_COMMAND . ':' . Constants::SYTE_PRODUCT_FEED_MODE_COMMAND
            . ':export')
            ->setDescription(__('Create and send Syte product feed'))
            ->setDefinition($this->getMakerOptions());
    }

    /**
     * @param Helper $helper
     */
    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
        parent::__construct();
    }

    /**
     * @return array
     */
    private function getOptions(): array
    {
        return ['store', 'no-connection'];
    }

    /**
     * Configure oftions for import command
     *
     * @return array
     */
    protected function getMakerOptions(): array
    {
        $options = [];
        foreach ($this->getOptions() as $key) {
            $options[] = new InputOption(
                $key,
                null,
                InputOption::VALUE_OPTIONAL,
                (string)__(ucfirst($key))
            );
        }

        return $options;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $createOnly = (int)$input->getOption('no-connection') > 0;
        $storeIds = $this->helper->getStoresList(
            $input->hasOption('store') ? (int)$input->getOption('store') : 0
        );
        if (empty($storeIds)) {
            $output->writeln('ERROR: ' . __('Cannot find a store'));
        } else {
            $result = false;
            foreach ($storeIds as $storeId) {
                if (!$this->helper->isServiceActive($storeId)) {
                    continue;
                }
                $result = true;
                $successMsg = [];
                $errorMsg = [];
                $this->helper->createAndSendProductFeeds($successMsg, $errorMsg, $storeId, $createOnly ? true : null);
                foreach ($successMsg as $msg) {
                    $output->writeln($msg);
                }
                foreach ($errorMsg as $msg) {
                    $output->writeln('ERROR: ' . $msg);
                }
            }
            if (!$result) {
                $output->writeln('WARNING: ' . __('This service is not available'));
            }
        }
        $output->writeln(__('Done'));
    }
}
