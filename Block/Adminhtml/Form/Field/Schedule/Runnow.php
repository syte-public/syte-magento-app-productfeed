<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Block\Adminhtml\Form\Field\Schedule;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

class Runnow extends Field
{
    /**
     * @inheritDoc
     */
    protected function _renderScopeLabel(AbstractElement $element): string
    {
        // Return empty label
        return '';
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        // Replace field markup with validation button
        $title = __('Run Now');
        $endpoint = $this->getUrl('syteproductfeed/configuration/validate');
        // @codingStandardsIgnoreStart
        $html = <<<TEXT
            <button
                    type="button"
                    name="syte-pf-run-now"
                    id="syte-pf-run-now"
                    title="{$title}"
                    class="button"
                    onclick="syteRunNow.call(this, '{$endpoint}')">
                    <span>{$title}</span>
            </button>
TEXT;
        // @codingStandardsIgnoreEnd

        return $html;
    }
}
