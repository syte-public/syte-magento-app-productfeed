<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Cron;

use Syte\ProductFeed\Model\Helper;

class Export
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @param Helper $helper
     */
    public function __construct(
        Helper $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * Create feed and export
     */
    public function execute()
    {
        $this->helper->executeExportBySchedule();
    }
}
