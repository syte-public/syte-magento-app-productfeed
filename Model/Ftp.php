<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Config\Model\Config\Backend\Encrypted;
use Magento\Framework\Filesystem\Io\Sftp;
use Magento\Framework\Filesystem\Io\File;
use Syte\ProductFeed\Model\Config;

class Ftp extends AbstractHelper
{
    /**
     * @var Config
     */
    private $configHelper;

    /**
     * @var Encrypted
     */
    private $encrypted;

    /**
     * @var Sftp
     */
    private $sftp;

    /**
     * @var File
     */
    private $fileIO;

    /**
     * @param Context $context
     * @param Config $configHelper
     * @param Encrypted $encrypted
     * @param Sftp $sftp
     * @param File $fileIO
     */
    public function __construct(
        Context $context,
        Config $configHelper,
        Encrypted $encrypted,
        Sftp $sftp,
        File $fileIO
    ) {
        $this->configHelper = $configHelper;
        $this->encrypted = $encrypted;
        $this->sftp = $sftp;
        $this->fileIO = $fileIO;
        parent::__construct($context);
    }

    /**
     * Check empty ftp credentials
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isFtpConfigNotEmpty(int $storeId): bool
    {
        $data = $this->configHelper->getFtpConfig($storeId);
        $this->modifyFtpConfig($storeId, $data);

        return $data['host'] && $data['port'] && $data['user'] && $data['password'];
    }

    /**
     * Modify ftp config values
     *
     * @param int $storeId
     * @param array $data
     * @param bool $encrypted
     *
     * @return void
     */
    private function modifyFtpConfig(int $storeId, array &$data, bool $encrypted = true)
    {
        //pwd
        $pwd = $data['password'];
        if ($encrypted) {
            $data['password'] = $this->encrypted->processValue($data['password']);
        } elseif (strpos($pwd, '*') === 0) {
            $dataSaved = $this->configHelper->getFtpConfig($storeId);
            $data['password'] = $this->encrypted->processValue($dataSaved['password']);
        }

        //port
        $data['port'] = (int)$data['port'] ?: Sftp::SSH2_PORT;
        //remote path
        $data['path'] = rtrim(trim((string)$data['path']), '/');
    }

    /**
     * Transfer file or test connection
     *
     * @param int $storeId
     * @param array $data
     * @param string $sourceFileName
     * @param string $remoteFileName
     *
     * @return string
     */
    public function executeConnection(
        int $storeId,
        array $data,
        string $sourceFileName = '',
        string $remoteFileName = ''
    ): string {
        $errorMsg = '';
        $testMode = $sourceFileName ? false : true;
        $ftpConfig = empty($data) ? $this->configHelper->getFtpConfig($storeId) : $data;
        $this->modifyFtpConfig($storeId, $ftpConfig, !$testMode);
        $args = [
            'host'     => sprintf('%s:%s', $ftpConfig['host'], $ftpConfig['port']),
            'username' => $ftpConfig['user'],
            'password' => $ftpConfig['password'],
        ];
        try {
            $this->sftp->open($args);
            if (!$testMode) {
                if ($remoteFileName) {
                    $basename = $remoteFileName;
                } else {
                    $fileInfo = $this->fileIO->getPathInfo($sourceFileName);
                    $basename = $fileInfo['basename'];
                }
                $remotePath = $ftpConfig['path'] . '/' . $basename;
                $this->sftp->write($remotePath, $sourceFileName);
            } elseif ($ftpConfig['path']) {
                $this->sftp->cd($ftpConfig['path']);
                if ((string)$this->sftp->pwd() !== $ftpConfig['path']) {
                    $errorMsg = 'Cannot find remote directory';
                }
            }
        } catch (\Exception $e) {
            $errorMsg = 'Unable to connect to remote host'; //$e->getMessage() may cause an exception
        } finally {
            $this->sftp->close();
        }

        return $errorMsg;
    }
}
