<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model\ResourceModel\Feed;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'feed_id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'syteproductfeed_feed_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'feed_collection';

    /**
     * Define model & resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Syte\ProductFeed\Model\Feed::class, \Syte\ProductFeed\Model\ResourceModel\Feed::class);
    }
}
