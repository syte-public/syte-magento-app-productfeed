<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Locale\Resolver;
use Magento\Catalog\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as TypeConfigurable;
use Magento\GroupedProduct\Model\Product\Type\Grouped as TypeGrouped;
use Magento\Bundle\Model\Product\Type as TypeBundle;
use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Directory\Model\Currency;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Syte\ProductFeed\Model\Config;
use Syte\Core\Model\Constants;

class FeedData extends AbstractHelper
{
    /**
     * @var Config
     */
    protected $configHelper;

    /**
     * @var Resolver
     */
    protected $locale;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var TypeConfigurable
     */
    protected $typeConfigurable;

    /**
     * @var TypeGrouped
     */
    protected $typeGrouped;

    /**
     * @var TypeBundle
     */
    protected $typeBundle;

    /**
     * @var StockStateInterface
     */
    protected $stockState;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var array
     */
    private $storeCache = [];

    /**
     * @param Context $context
     * @param Config $configHelper
     * @param Resolver $locale
     * @param ProductFactory $productFactory
     * @param TypeConfigurable $typeConfigurable
     * @param TypeGrouped $typeGrouped
     * @param TypeBundle $typeBundle
     * @param StockStateInterface $stockState
     * @param Currency $currency
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Context $context,
        Config $configHelper,
        Resolver $locale,
        ProductFactory $productFactory,
        TypeConfigurable $typeConfigurable,
        TypeGrouped $typeGrouped,
        TypeBundle $typeBundle,
        StockStateInterface $stockState,
        Currency $currency,
        CategoryCollectionFactory $categoryCollectionFactory,
        SerializerInterface $serializer
    ) {
        $this->configHelper = $configHelper;
        $this->locale = $locale;
        $this->productFactory = $productFactory;
        $this->typeConfigurable = $typeConfigurable;
        $this->typeGrouped = $typeGrouped;
        $this->typeBundle = $typeBundle;
        $this->stockState = $stockState;
        $this->currency = $currency;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->serializer = $serializer;
        parent::__construct($context);
    }

    /**
     * Convert string to utf8
     *
     * @param mixed $text
     *
     * @return mixed
     */
    private function toUtf8($obj)
    {
        $conv = is_string($obj) ? iconv(mb_detect_encoding($obj, mb_detect_order(), true), 'UTF-8', $obj) : false;

        return $conv ?: $obj;
    }

    /**
     * Reset store cached data
     *
     * @param int $storeId
     *
     * @return array
     */
    private function invalidateStoreCache(int $storeId): void
    {
        $this->storeCache[$storeId] = [
                'currency' => '',
                'locale' => '',
                'categories' => [],
            ];
    }

    /**
     * Get product row
     *
     * @param int $storeId
     * @param int $page
     *
     * @return array
     */
    public function getFeedRows(int $storeId, int $page = 1): array
    {
        $this->logDebug(__METHOD__);
        $this->logDebug("Store Id # ".$storeId);

        if ((1 == $page) || !isset($this->storeCache[$storeId])) {
            $this->invalidateStoreCache($storeId);
        }

        try {
            $data = [];
            $attributesAdd = ['id', 'visibility', 'status', 'price'];
            $attributes = $this->getMappingDataByName($storeId, 'attribute');
            $attrDefaultValues = $this->getMappingDataByName($storeId, 'value');
            $this->logDebug($attrDefaultValues, 1);
            $blockSize = (int)$this->configHelper->getFeedFileConfig('block_size', $storeId) ?: 200;
            $products = $this->productFactory->create()
                ->getCollection()
                ->setStoreId($storeId)
                ->setPageSize($blockSize)
                ->setCurPage($page)
                ->addAttributeToSelect(array_merge($attributesAdd, $attributes));
            if ($this->needMediaData($attributes)) {
                $products->addMediaGalleryData();
            }
//            $products->setPage($page, $blockSize);

            if ($page <= $products->getLastPageNumber()) {
                $this->logDebug("Page:".$page);
                $this->logDebug("BlockSize:".$blockSize);

                $this->logDebug(__LINE__."Product loop:: Start...");
                foreach ($products as $product) {
                    $this->logDebug("ProdId: ".$product->getId());
                    if($product->getId()>500){
                        break; // Temp termination/limit of loop TODO: REmove after debug
                    }

                    $product->setStoreId($storeId);
                    $row = [];

                    foreach ($attributes as $idx => $attribute) {
                        $value = $attribute ? $this->getProductAttributeValue($product, $attribute) : ''; // ??? Expensive
                        $row[] = $this->toUtf8($value ?: $attrDefaultValues[$idx]);
                    }

                    $this->logDebug($row,1);
                    $data[] = $row;
                }
                $this->logDebug(__LINE__."End of product loop");
            }
        }catch (\Exception $exception){
            $this->logDebug($exception->getMessage(), 1);

        }
        $this->logDebug(__LINE__);
        return $data;
    }

    /**
     * Get feed column headers
     *
     * @param int $storeId
     *
     * @return array
     */
    public function getFeedHeaders(int $storeId): array
    {
        return $this->getMappingDataByName($storeId, 'name');
    }

    /**
     * Returns true if mediagallery data is required
     *
     * @param array $attributes
     *
     * @return bool
     */
    private function needMediaData(array $attributes): bool
    {
        foreach ($attributes as $attr) {
            if (false !== strpos($attr, Constants::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get feed filename
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getFeedFileName(int $storeId): string
    {
        $filename = time();
        $this->locale->emulate($storeId);
        if ($locale = $this->locale->getLocale()) {
            $filename = $locale . '_' . $filename;
        }
        $this->locale->revert();
        if ($accountName = $this->configHelper->getAccountName($storeId)) {
            $filename = $accountName . '_' . $filename;
        }

        return $filename . '.' . Constants::SYTE_PRODUCT_FEED_FILE_EXTENSION;
    }

    /**
     * Get mapping data
     *
     * @param int $storeId
     *
     * @return string
     */
    private function getMappingData(int $storeId): array
    {
        $data = $this->configHelper->getFeedFileConfig('mapping', $storeId);
        if (!is_array($data)) {
            $data = $this->serializer->unserialize($data);
        }
        if (!is_array($data)) {
            return [];
        } elseif (!empty($data)) {
            usort($data, $this->callbackSorter('sort'));
        }

        return $data;
    }

     /**
      * Get mapping data for specified key
      *
      * @param int $storeId
      * @param string $key
      *
      * @return string
      */
    private function getMappingDataByName(int $storeId, string $key): array
    {
        $data = [];
        foreach ($this->getMappingData($storeId) as $value) {
            $data[] = (isset($value[$key]) && (string)$value[$key]) ? trim((string)$value[$key]) : '';
        }

        return $data;
    }

    /**
     * Sorter callback.
     *
     * @param string $key
     */
    private function callbackSorter(string $key)
    {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);
        };
    }

    /**
     * Get product attribute value
     *
     * @param Product $product
     * @param string $attributeCode
     *
     * @return string
     */
    private function getProductAttributeValue($product, string $attributeCode): string
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $attributeModel = $product->getResource()->getAttribute($attributeCode);
        if ($product->getData($attributeCode) && $attributeModel
            && $attributeModel->getFrontendInput() == 'select' && $product->getData($attributeCode))
        {
            $attributeValue = $product->getAttributeText($attributeCode);
            if ($attributeValue instanceof \Magento\Framework\Phrase) {
                return $attributeValue->getText();
            }elseif (is_string($attributeValue)) {
                return $attributeValue;
            }
        }

        $value = (string)$product->getData($attributeCode);

        if ('' === $value) {
            $value = $this->getProductCustomValue($product, $attributeCode);
        }

        return $value;
    }

    /**
     * Get product custom value
     *
     * @param Product $product
     * @param string $attributeCode
     *
     * @return string
     */
    private function getProductCustomValue($product, string $attributeCode): string
    {
        $value = '';
        $isCustom = false;
        foreach (Constants::SYTE_PRODUCT_FEED_ATTRIBUTES as $key => $attrName) {
            $isCustom = $isCustom || ($attributeCode === $key);
        }
        if (!$isCustom) {
            return $value;
        }
        switch ($attributeCode) {
            case Constants::SYTE_PRODUCT_FEED_ATTR_PARENT_SKU:
                $value = $this->getProductCustomParentSku($product);
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_PRODUCT_URL:
                $value = $this->getProductCustomUrl($product);
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL:
            case Constants::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_1:
            case Constants::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_2:
            case Constants::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_3:
                $imageNum = str_replace(Constants::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL, '', $attributeCode);
                $imageNum = str_replace('_', '', $imageNum);
                $value = $this->getProductCustomImageUrl($product, (int)$imageNum);
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_CATEGORY:
                $value = $this->getProductCustomCategory($product);
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_QTY:
                $value = $this->getProductCustomQty($product);
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_PRICE:
                $value = $this->getProductCustomPrice($product);
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_CURRENCY:
                $value = $this->getProductCustomCurrency($product->getStore());
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_LOCALE:
                $value = $this->getProductCustomLocale($product->getStoreId());
                break;
            case Constants::SYTE_PRODUCT_FEED_ATTR_PRODUCT_TYPE:
                $value = $product->getTypeId();
                break;
        }

        return $value;
    }

    /**
     * Get all categories data
     *
     * @param int $storeId
     *
     * @return array
     */
    private function getAllStoreCategories(int $storeId): array
    {
        $data = [];
        $categories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect(['id', 'name', 'path'])
            ->setStore($storeId);
        foreach ($categories as $category) {
            if ($id = $category->getId()) {
                $data[$id] = [];
                $data[$id]['name'] = $category->getName();
                $data[$id]['path'] = $category->getPath();
            }
        }

        return $data;
    }

    /**
     * Get product visibility
     *
     * @param Product $product
     *
     * @return bool
     */
    private function isProductVisibile($product): bool
    {
        return in_array((int)$product->getVisibility(), [
            Visibility::VISIBILITY_IN_CATALOG,
            Visibility::VISIBILITY_BOTH
        ]);
    }

    /**
     * Get flag for simple product
     *
     * @param Product $product
     *
     * @return bool
     */
    private function isProductSimple($product): bool
    {
        return !in_array($product->getTypeId(), [
            TypeGrouped::TYPE_CODE,
            TypeBundle::TYPE_CODE,
            TypeConfigurable::TYPE_CODE,
        ]);
    }

    /**
     * Get product parent
     *
     * @param Product $product
     *
     * @return Product|null
     */
    private function getProductCustomParent($product)
    {
        $id = $product->getId();
        $parentId = 0;
        $parentByChild = $this->typeConfigurable->getParentIdsByChild($id);
        if (isset($parentByChild[0])) {
            $parentId = $parentByChild[0];
        } elseif (!$this->isProductVisibile($product)) {
            $parentByChild = $this->typeGrouped->getParentIdsByChild($id);
            if (isset($parentByChild[0])) {
                $parentId = $parentByChild[0];
            } else {
                $parentByChild = $this->typeBundle->getParentIdsByChild($id);
                if (isset($parentByChild[0])) {
                    $parentId = $parentByChild[0];
                }
            }
        }
        if ($parentId) {
            if ($parentProduct = $this->productFactory->create()->load($parentId)) {
                return $parentProduct;
            }
        }

        return null;
    }

    /**
     * Get product parent sku
     *
     * @param Product $product
     * @param bool $useOwnSkuIfNoParent
     *
     * @return string
     */
    private function getProductCustomParentSku($product, bool $useOwnSkuIfNoParent = false): string
    {
        $parentProduct = $this->getProductCustomParent($product);
        $sku = ($parentProduct && $parentProduct->getId()) ? (string)$parentProduct->getSku() : '';

        return $sku ?: ($useOwnSkuIfNoParent ? $product->getSku() : '');
    }

    /**
     * Get product url
     *
     * @param Product $product
     * @param bool $checkVisibleOnly
     *
     * @return string
     */
    private function getProductCustomUrl($product, bool $checkVisibleOnly = true): string
    {
        $url = '';
        $allowed = !$checkVisibleOnly || ($this->isProductVisibile($product) && $product->getStatus());
        if (!$allowed && $parentProduct = $this->getProductCustomParent($product)) {
            if ($parentProduct->getId()) {
                return $this->getProductCustomUrl($parentProduct);
            }
        }

        return $allowed ? $product->getUrlModel()->getUrlInStore($product, ['_escape' => true]) : '';
    }

    /**
     * Get product image url
     *
     * @param Product $product
     * @param int $imageNum
     *
     * @return string
     */
    private function getProductCustomImageUrl($product, int $imageNum): string
    {
        $productimages = $product->getMediaGalleryImages();
        $urls = [];
        foreach ($productimages as $productimage) {
            if ($url = $productimage['url']) {
                $urls[] = $url;
            }
        }

        return isset($urls[$imageNum]) ? $urls[$imageNum] : '';
    }

    /**
     * Get product category
     *
     * @param Product $product
     *
     * @return string
     */
    private function getProductCustomCategory($product): string
    {
        $path = [];
        $tag = 'categories';
        $storeId = (int)$product->getStoreId();
        $categoryIds = $product->getCategoryIds();
        if (!empty($categoryIds)) {
            if (empty($this->storeCache[$storeId][$tag])) {
                $this->storeCache[$storeId][$tag] = $this->getAllStoreCategories($storeId);
            }
            foreach ($categoryIds as $id) {
                if (isset($this->storeCache[$storeId][$tag][$id])) {
                    $catPath = explode('/', $this->storeCache[$storeId][$tag][$id]['path']);
                    $pathNames = [];
                    foreach ($catPath as $idx => $pathId) {
                        if ($idx <= 1) {
                            continue;
                        }
                        if (isset($this->storeCache[$storeId][$tag][$pathId])) {
                            $pathNames[] = $this->storeCache[$storeId][$tag][$pathId]['name'];
                        }
                    }
                    if (!empty($pathNames)) {
                        $path[]  = $pathNames;
                    }
                }
            }
        }
        $length = 0;
        $result = '';
        foreach ($path as $pathNames) {
            $cnt = count($pathNames);
            if ($cnt > $length) {
                $length = $cnt;
                $result = implode('>', $pathNames);
            }
        }

        return $result;
    }

    /**
     * Get product stock qty
     *
     * @param Product $product
     *
     * @return string
     */
    private function getProductCustomQty($product): string
    {
        return (string)$this->stockState->getStockQty(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        );
    }

    /**
     * Get product price
     *
     * @param Product $product
     *
     * @return string
     */
    private function getProductCustomPrice($product): string
    {
        return $this->isProductSimple($product)
            ? $this->currency->format($product->getPrice(), ['symbol' => ''], false, false)
            : '';
    }

    /**
     * Get currency
     *
     * @param Store $store
     *
     * @return string
     */
    private function getProductCustomCurrency($store): string
    {
        $storeId = (int)$store->getId();
        $tag = 'currency';
        if (!$this->storeCache[$storeId][$tag]) {
            $this->storeCache[$storeId][$tag] = (string)$store->getCurrentCurrency()->getCode();
        }

        return $this->storeCache[$storeId][$tag];
    }

    /**
     * Get language code
     *
     * @param int $storeId
     *
     * @return string
     */
    private function getProductCustomLocale(int $storeId): string
    {
        $tag = 'locale';
        if (!$this->storeCache[$storeId][$tag]) {
            $this->locale->emulate($storeId);
            $this->storeCache[$storeId][$tag] = $this->locale->getLocale();
            $this->locale->revert();
        }

        return $this->storeCache[$storeId][$tag];
    }


    // @TODO Remove after debugging
    protected function logDebug($message, $encode= false, $isWarning = false, $backTrace = false)
    {
        $class_name = get_class($this);
        $fileName = strtolower(str_replace('\\', '_', $class_name)) . '.log';
        $filePath = BP . '/var/log/debug_' . $fileName;

        $writer = new \Laminas\Log\Writer\Stream($filePath);
        $logger = new \Laminas\Log\Logger();
        $logger->addWriter($writer);

        if ($encode) {
            $message = json_encode($message);
        }

        if ($isWarning) {
            $logger->warn($message);
        } else {
            $logger->info($message);
        }

        if ($backTrace) {
            $debugBackTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            $logger->debug(json_encode($debugBackTrace));
        }
    }
}
