<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Filesystem\DriverInterface\Proxy as DriverInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Exception\LocalizedException;
use Syte\ProductFeed\Model\FeedFactory;
use Syte\ProductFeed\Api\FeedRepositoryInterface;
use Syte\ProductFeed\Model\Config;
use Syte\ProductFeed\Model\Ftp;
use Syte\ProductFeed\Model\FeedData\Proxy as FeedData;
use Psr\Log\LoggerInterface;
use Syte\Core\Model\Constants;
use Syte\Core\Model\Events\Tracker;

class Helper extends AbstractHelper
{
    /* @const int */
    private const EVENT_CREATION_START = 1;
    private const EVENT_CREATION_COMPLETED = 2;
    private const EVENT_CREATION_ERROR = 3;
    private const EVENT_UPLOAD_START = 4;
    private const EVENT_UPLOAD_COMPLETED = 5;
    private const EVENT_UPLOAD_ERROR = 6;

    /**
     * @var Config
     */
    private $configHelper;

    /**
     * @var Ftp
     */
    private $ftpHelper;

    /**
     * @var FeedData
     */
    private $dataHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var DirectoryList
     */
    private $directoryList;

     /**
      * @var Csv
      */
    private $csvProcessor;

    /**
     * @var File
     */
    private $fileIO;

    /**
     * @var FeedFactory
     */
    private $feedFactory;

    /**
     * @var FeedRepositoryInterface
     */
    private $feedRepository;

    /**
     * @var DriverInterface
     */
    private $driverInterface;

    /**
     * @var Tracker
     */
    private $tracker;

    /**
     * @var array
     */
    private $exportErrors = [];

    /**
     * @var string
     */
    private $jobStartTime = null;

    /**
     * @param Context $context
     * @param Config $configHelper
     * @param FeedData $dataHelper
     * @param Ftp $ftpHelper
     * @param LoggerInterface $logger
     * @param StoreManagerInterface $storeManager
     * @param DirectoryList $directoryList
     * @param Csv $csvProcessor
     * @param File $fileIO
     * @param FeedFactory $feedFactory
     * @param FeedRepositoryInterface $feedRepository
     * @param DriverInterface $driverInterface
     * @param Tracker $tracker
     */
    public function __construct(
        Context $context,
        Config $configHelper,
        FeedData $dataHelper,
        Ftp $ftpHelper,
        LoggerInterface $logger,
        StoreManagerInterface $storeManager,
        DirectoryList $directoryList,
        Csv $csvProcessor,
        File $fileIO,
        FeedFactory $feedFactory,
        FeedRepositoryInterface $feedRepository,
        DriverInterface $driverInterface,
        Tracker $tracker
    ) {
        $this->configHelper = $configHelper;
        $this->dataHelper = $dataHelper;
        $this->ftpHelper = $ftpHelper;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->directoryList = $directoryList;
        $this->csvProcessor = $csvProcessor;
        $this->fileIO = $fileIO;
        $this->feedFactory = $feedFactory;
        $this->feedRepository = $feedRepository;
        $this->driverInterface = $driverInterface;
        $this->tracker = $tracker;
        parent::__construct($context);
    }

    /**
     * Execute service for specified store
     *
     * @param int $storeId
     * @param string $errorMsg
     * @param null|bool $createOnly
     *
     * @return string
     */
    private function createAndSendProductFeed(
        int $storeId,
        string &$errorMsg,
        $createOnly = null
    ): string {
        $this->logDebug(__METHOD__);

        $filepath = '';
        $this->exportErrors = [];
        $connectionType = $this->configHelper->getConnectionType($storeId);
        $ignoreSend = (null === $createOnly) ? (Constants::SYTE_CONFIG_CONNECTION_NONE == $connectionType)
            : $createOnly;
        if ($this->validateServiceParams($storeId, $ignoreSend)) {
            if ($filepath = $this->createFeedFile($storeId)) {
                if (!$ignoreSend) {
                    $this->sendFeedFile($storeId, $filepath);
                }
            }
        }
        $errorMsg = empty($this->exportErrors) ? '' : array_shift($this->exportErrors);
        //add history
        if (!$errorMsg) {
            try {
                $history = $this->feedFactory->create();
                $history->setStoreId($storeId);
                $history->setFilename($this->getFileBaseName($filepath));
                $history->setStatus($ignoreSend ? 'Created' : 'Exported');
                if ($this->jobStartTime) {
                    $history->setJobStartedAt($this->jobStartTime);
                }
                $this->feedRepository->save($history);
            } catch (\Exception $e) {
                $errorMsg = __('Unable to create history record');
                $this->logDebug($errorMsg,1);
            }
        }

        return $filepath;
    }

    /**
     * Execute service for all stores
     *
     * @param array $successMsg
     * @param array $errorMsg
     * @param int $storeId
     * @param null|bool $createOnly
     *
     * @return void
     */
    public function createAndSendProductFeeds(
        array &$successMsg,
        array &$errorMsg,
        int $storeId = 0,
        $createOnly = null
    ): bool {
        $this->logDebug(__METHOD__);
        $result = false;
        $this->jobStartTime = date('Y-m-d H:i:s');
        $successMsg = [];
        $errorMsg = [];
        $storeIds = $this->getStoresList($storeId);
        $this->logDebug("storeIds :");
        $this->logDebug($storeIds,1);
        if (empty($storeIds)) {
            $errorMsg[] = __('Cannot find a store');
        } else {
            $this->logDebug(__LINE__);
            try {


                foreach ($storeIds as $storeId) {
                    if (!$this->isServiceActive($storeId)) {
                        continue;
                    }
                    if (!$result && $allowLog = $this->configHelper->isLogActive($storeId)) {
                        $this->addDebugInfo((string)__('Scheduled job started'));
                        $this->logDebug('Scheduled job started');
                    }
                    $result = true;
                    $error = '';
                    $this->logDebug(__LINE__);
                    $filename = $this->createAndSendProductFeed($storeId, $error, $createOnly);
                    $this->logDebug(__LINE__);
                    if ($error) {
                        if ($filename) {
                            $successMsg[] = __(
                                'Product Feed for store %1 has been created with errors (%2)',
                                $storeId,
                                $filename
                            );
                        }
                        $errorMsg[] = $error;
                    } elseif (!$filename) {
                        $errorMsg[] = __('Source data for store %1 is empty', $storeId);
                    } else {
                        $successMsg[] = __(
                            'Product Feed for store %1 has been processed successfully (%2)',
                            $storeId,
                            $filename
                        );
                    }
                }

            }catch (\Exception $exception){
                $this->logDebug($exception->getMessage(),1);
            }
        }
        if ($result && $allowLog) {
            foreach ($successMsg as $msg) {
                $this->addDebugInfo((string)$msg);
            }
            foreach ($errorMsg as $msg) {
                $this->addDebugInfo((string)$msg, true);
            }
        }

        return $result;
    }

    /**
     * Add debug log
     *
     * @param mixed $data
     * @param bool $isError
     *
     * @return string
     */
    private function addDebugInfo($data, bool $isError = false)
    {
        $this->logger->debug(
            'Syte_ProductFeed ' . ($isError ? 'ERROR' : 'INFO'),
            is_array($data) ? $data : (array)$data
        );
    }

    /**
     * Get source folder
     *
     * @param bool $isCompleted
     *
     * @return string
     */
    private function getSourceFeedFolder(bool $isCompleted = false): string
    {
        $this->logDebug(__METHOD__);
        $fileDirectoryPath = rtrim($this->directoryList->getPath(DirectoryList::VAR_DIR), '/') . '/'
            . ($isCompleted ? Constants::SYTE_PRODUCT_FEED_COMPLETED_PATH : Constants::SYTE_PRODUCT_FEED_PATH);
        if (!$this->driverInterface->isDirectory($fileDirectoryPath)) {
            if (!$this->fileIO->mkdir($fileDirectoryPath)) {
                $fileDirectoryPath = '';
            }
        }
        $this->logDebug($fileDirectoryPath);
        return $fileDirectoryPath;
    }

    /**
     * Create csv feed
     *
     * @param int $storeId
     *
     * @return string
     */
    private function createFeedFile(int $storeId): string
    {
        $this->logDebug(__METHOD__);
        $fileName = $this->dataHelper->getFeedFileName($storeId);
        $this->logDebug("FileName : ".$fileName);
        $this->dispatchFeedEvent(self::EVENT_CREATION_START, $fileName, $storeId);
        try {


            if (!$fileDirectoryPath = $this->getSourceFeedFolder()) {
                $e = __('Cannot create source folder');
                $this->logDebug($e);
                $this->exportErrors[] = $e;
                $this->dispatchFeedEvent(self::EVENT_CREATION_ERROR, $fileName, $storeId, (string)$e);
                return '';
            }
            $filePath = $fileDirectoryPath . '/' . $fileName;
            $this->logDebug("FilePath : " . $filePath);
            $this->fileIO->rm($filePath);
            if ($this->fileIO->fileExists($filePath)) {
                $e = __('Cannot overwrite source file');
                $this->exportErrors[] = $e;
                $this->logDebug($e);
                $this->dispatchFeedEvent(self::EVENT_CREATION_ERROR, $filePath, $storeId, (string)$e);
                return '';
            }

            $offset = 0;
            $this->csvProcessor->setDelimiter(',')->setEnclosure('"');
            do {
                $data = [];
                if (!$offset) {
                    $data[] = $this->dataHelper->getFeedHeaders($storeId);
                }
                $this->logDebug($data , 1);

                $this->logDebug("Getting Row data ..... ");

                $rows = $this->dataHelper->getFeedRows($storeId, $offset + 1);
                $this->logDebug("Rows Size:");
                $this->logDebug(count($rows) , 1);

                if (!empty($rows)) {
                    foreach ($rows as $row) {
                        $data[] = $row;
                        $this->logDebug($row , 1);
                    }
                    try {
                        $this->csvProcessor->appendData($filePath, $data, $offset ? 'a' : 'w');
                        if (!$this->fileIO->fileExists($filePath)) {
                            $filePath = '';
                            $this->exportErrors[] = __('Csv processor failed');
                            break;
                        }
                    } catch (\Exception $e) {
                        $filePath = '';
                        $this->exportErrors[] = __('Cannot create source file');
                        break;
                    }
                    $offset++;
                    // @codingStandardsIgnoreLine
                    sleep(1); // recommended to avoid specific server's issues
                    $this->logDebug($this->exportErrors, 1);
                }
            } while (!empty($rows) && $filePath);

            $e = empty($this->exportErrors) ? '' : $this->exportErrors[count($this->exportErrors) - 1];
            $this->dispatchFeedEvent(
                $e ? self::EVENT_CREATION_ERROR : self::EVENT_CREATION_COMPLETED,
                $filePath,
                $storeId,
                (string)$e
            );

        }catch (\Exception $exception){
            $this->logDebug($exception->getMessage(), 1);
        }

        return $filePath;
    }

    /**
     * Send csv feed
     *
     * @param int $storeId
     * @param string $filepath
     *
     * @return bool
     */
    private function sendFeedFile(int $storeId, string &$filepath): bool
    {
        $this->logDebug(__METHOD__);
        $result = false;
        $error = '';
        $connectionType = $this->configHelper->getConnectionType($storeId);
        switch ($connectionType) {
            case Constants::SYTE_CONFIG_CONNECTION_SFTP:
                $this->dispatchFeedEvent(self::EVENT_UPLOAD_START, $filepath, $storeId);
                if ((int)$this->configHelper->getConfigValue(
                    Constants::SYTE_CONFIG_PATH_PRODUCT_FEED . 'ftp_compress',
                    $storeId
                )
                ) {
                    $fileZip = $this->packFeedFile($filepath);
                } else {
                    $fileZip = '';
                }
                if ($error = $this->ftpHelper->executeConnection($storeId, [], $fileZip ?: $filepath)) {
                    $e = __($error);
                    $this->exportErrors[] = $e;
                    $this->dispatchFeedEvent(self::EVENT_UPLOAD_ERROR, $filepath, $storeId, (string)$e);
                } else {
                    $result = true;
                    $this->dispatchFeedEvent(self::EVENT_UPLOAD_COMPLETED, $fileZip ?: $filepath, $storeId);
                }
                if ($fileZip) {
                    $this->fileIO->rm($fileZip);
                }
                break;
        }
        if ($result) {
            $this->moveFeedFileToExecuted($filepath);
        } elseif (!$error) {
            $this->exportErrors[] = __('Cannot send source file');
        }

        return $result;
    }

    /**
     * Pack csv to zip
     *
     * @param string $filepath
     *
     * @return string
     */
    private function packFeedFile(string $filepath): string
    {
        try {
            $fileZip = preg_replace(
                '"\.' . Constants::SYTE_PRODUCT_FEED_FILE_EXTENSION . '$"',
                '.zip',
                $filepath
            );
            $zip = new \ZipArchive();
            $zip->open($fileZip, \ZipArchive::CREATE);
            $zip->addFile($filepath, $this->getFileBaseName($filepath));
            $zip->close();
        } catch (\Exception $e) {
            $fileZip = '';
            throw new LocalizedException(__('We can\'t compress source file.'));
        }

        return $fileZip;
    }

    /**
     * Move csv feed to executed storage
     *
     * @param string $filepath
     *
     * @return bool
     */
    private function moveFeedFileToExecuted(string &$filepath): bool
    {
        $result = false;
        if ($filepath && $newDirectoryPath = $this->getSourceFeedFolder(true)) {
            $newpath = $newDirectoryPath . '/' . $this->getFileBaseName($filepath);
            if ($result = $this->fileIO->mv($filepath, $newpath)) {
                $filepath = $newpath;
            }
        }

        return $result;
    }

     /**
      * Validate necessary config settings
      *
      * @param int $storeId
      * @param bool $createOnly
      *
      * @return bool
      */
    private function validateServiceParams(int $storeId, bool $createOnly): bool
    {
        $error = '';
        if (!$this->configHelper->isAccountActive($storeId)) {
            $error = __('Account is not active');
        } elseif (!$this->configHelper->isServiceActive($storeId)) {
            $error = __('Service is not active');
        } elseif (!$createOnly) {
            $connectionType = $this->configHelper->getConnectionType($storeId);
            if (Constants::SYTE_CONFIG_CONNECTION_NONE == $connectionType) {
                $error = __('Connection is not configured');
            } elseif ((Constants::SYTE_CONFIG_CONNECTION_SFTP == $connectionType)
                && !$this->ftpHelper->isFtpConfigNotEmpty($storeId)
            ) {
                $error = __('Cannot validate SFTP settings');
            }
        }
        if ($error) {
            $this->exportErrors[] = $error;
        }

        return $error ? false : true;
    }

    /**
     * Get ids of available stores
     *
     * @param int $storeId
     *
     * @return array
     */
    public function getStoresList(int $storeId = 0): array
    {
        $data = [];
        foreach ($this->storeManager->getStores() as $key => $value) {
            if ($id = (int)$value['store_id']) {
                if ($storeId) {
                    if ($storeId == $id) {
                        return [$id];
                    }
                } else {
                    $data[] = $id;
                }
            }
        }

        return $data;
    }

    /**
     * Get file basename
     *
     * @param string $filename
     *
     * @return string
     */
    public function getFileBaseName(string $filename): string
    {
        $fileInfo = $this->fileIO->getPathInfo($filename);

        return $fileInfo['basename'];
    }

    /**
     * Get active status
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isServiceActive(int $storeId): bool
    {
        return $this->configHelper->isServiceActive($storeId)
            && $this->configHelper->isAccountActive($storeId);
    }

    /**
     * Execute allaround export by schedule settings
     *
     * @param bool $checkScheduleOnly
     *
     * @return int (# exit code - shouldn't use translations, for console debug only)
     */
    public function executeExportBySchedule(bool $checkScheduleOnly = false): int
    {
        $scheduleData = $this->configHelper->getScheduleConfig();
        if (Constants::SYTE_SCHEDULE_ONCE == $scheduleData['schedule_type']) {
            $date = strtotime($scheduleData['schedule_date']);
            if (date('Y', $date) !== date('Y')) {
                return 2;
            }
        } elseif (Constants::SYTE_SCHEDULE_INTERVAL == $scheduleData['schedule_type']) {
            $dateFrom = strtotime($scheduleData['schedule_from']);
            $dateTo = $scheduleData['schedule_to'] ? strtotime($scheduleData['schedule_to']) : 0;
            $now = strtotime(date('Y-m-d'));
            if (($now < $dateFrom) || ($dateTo && ($now > $dateTo))) {
                return 3;
            }
        } else {
            return 1;
        }
        if (!$checkScheduleOnly) {
            $successMsg = [];
            $errorMsg = [];
            if (!$this->createAndSendProductFeeds($successMsg, $errorMsg)) {
                return 4;
            }
        }

        return 0;
    }

    /**
     * Dispatch custom event
     *
     * @param int $event
     * @param string $feedPath
     * @param int $storeId
     * @param string $errorMessage
     *
     * @return void
     */
    private function dispatchFeedEvent(
        int $event,
        string $feedPath,
        int $storeId,
        string $errorMessage = ''
    ): void {
        $feedName = $this->getFileBaseName($feedPath);
        switch ($event) {
            case self::EVENT_CREATION_START:
                $this->tracker->feedCreationStart($feedName, $storeId);
                break;
            case self::EVENT_CREATION_COMPLETED:
                $this->tracker->feedCreationCompleted($feedName, $storeId);
                break;
            case self::EVENT_CREATION_ERROR:
                $this->tracker->feedCreationFailed($feedName, $errorMessage, $storeId);
                break;
            case self::EVENT_UPLOAD_START:
                $this->tracker->feedUploadStart($feedName, $storeId);
                break;
            case self::EVENT_UPLOAD_COMPLETED:
                $this->tracker->feedUploadCompleted($feedName, $storeId);
                break;
            case self::EVENT_UPLOAD_ERROR:
                $this->tracker->feedUploadFailed($feedName, $errorMessage, $storeId);
                break;
        }
    }


    // @TODO Remove after debugging
    protected function logDebug($message, $encode= false, $isWarning = false, $backTrace = false)
    {
        $class_name = get_class($this);
        $fileName = strtolower(str_replace('\\', '_', $class_name)) . '.log';
        $filePath = BP . '/var/log/debug_' . $fileName;

        $writer = new \Laminas\Log\Writer\Stream($filePath);
        $logger = new \Laminas\Log\Logger();
        $logger->addWriter($writer);

        if ($encode) {
            $message = json_encode($message);
        }

        if ($isWarning) {
            $logger->warn($message);
        } else {
            $logger->info($message);
        }

        if ($backTrace) {
            $debugBackTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            $logger->debug(json_encode($debugBackTrace));
        }
    }
}
