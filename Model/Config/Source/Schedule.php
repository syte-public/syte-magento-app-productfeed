<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Syte\Core\Model\Constants;

class Schedule implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => Constants::SYTE_SCHEDULE_NONE, 'label' => __('None')],
            ['value' => Constants::SYTE_SCHEDULE_ONCE, 'label' => __('Run Once')],
            ['value' => Constants::SYTE_SCHEDULE_INTERVAL, 'label' => __('Recurring Interval')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            Constants::SYTE_SCHEDULE_NONE => __('None'),
            Constants::SYTE_SCHEDULE_ONCE => __('Run Once'),
            Constants::SYTE_SCHEDULE_INTERVAL => __('Recurring Interval'),
        ];
    }
}
