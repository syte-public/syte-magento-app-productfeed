<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Api;

use Syte\ProductFeed\Api\Data\FeedInterface;

interface FeedRepositoryInterface
{
    /**
     * Save entity
     *
     * @param FeedInterface $entity
     *
     * @return FeedInterface
     */
    public function save(FeedInterface $entity): FeedInterface;

    /**
     * Get entity by id
     *
     * @param int $id
     *
     * @return FeedInterface
     */
    public function getById(int $id): FeedInterface;

    /**
     * Delete entity
     *
     * @param FeedInterface $entity
     *
     * @return bool
     */
    public function delete(FeedInterface $entity): bool;

    /**
     * Delete entity by id
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteById(int $id): bool;
}
